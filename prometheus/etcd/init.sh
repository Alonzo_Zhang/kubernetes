kubectl -n p8s create secret generic etcd-certs --from-file=/opt/kubernetes/ssl/etcd/server.pem --from-file=/opt/kubernetes/ssl/etcd/server-key.pem --from-file=/opt/kubernetes/ssl/etcd/ca.pem
